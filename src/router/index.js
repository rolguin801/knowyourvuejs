import Vue from 'vue'
import VueRouter from 'vue-router'
import TestingView from '@/views/testing-view/testing.view';
import NumberView from '@/views/number-view/number.view';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'NumberView',
        component: NumberView
    },
    {
        path: '/testing-view',
        name: 'TestingView',
        component: TestingView
    }
];

const router = new VueRouter({
    routes
});

export default router
